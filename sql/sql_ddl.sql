--------------------------------------------------------
--  File created - Friday-November-04-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table CATEGORY
--------------------------------------------------------

  CREATE TABLE "43714994"."CATEGORY" 
   (	"CAT_ID" NUMBER(*,0), 
	"CAT_NAME" VARCHAR2(40 BYTE), 
	"CAT_DESC" VARCHAR2(128 BYTE) DEFAULT NULL, 
	"CAT_IMG_URL" VARCHAR2(128 BYTE) DEFAULT NULL, 
	"CAT_DISP_CMD" VARCHAR2(128 BYTE) DEFAULT null
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table SESSIONS
--------------------------------------------------------

  CREATE TABLE "43714994"."SESSIONS" 
   (	"ID" NUMBER(*,0), 
	"SHOPPER_ID" NUMBER(*,0), 
	"DATA" BLOB, 
	"TIME" TIMESTAMP (6)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" 
 LOB ("DATA") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING ) ;
--------------------------------------------------------
--  DDL for Table SHOPPER
--------------------------------------------------------

  CREATE TABLE "43714994"."SHOPPER" 
   (	"SHOPPER_ID" NUMBER(*,0), 
	"MARK_ID" NUMBER(*,0), 
	"SH_USERNAME" VARCHAR2(30 BYTE), 
	"SH_PASSWORD" CHAR(60 BYTE), 
	"SH_EMAIL" VARCHAR2(64 BYTE), 
	"SH_PHONE" VARCHAR2(45 BYTE), 
	"SH_TYPE" CHAR(1 BYTE), 
	"SH_SHOPGRP" NUMBER(*,0), 
	"SH_FIELD1" VARCHAR2(128 BYTE), 
	"SH_FIELD2" VARCHAR2(128 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table STORECONTACT
--------------------------------------------------------

  CREATE TABLE "43714994"."STORECONTACT" 
   (	"SC_NUM_ID" NUMBER(*,0), 
	"MARK_ID" NUMBER(*,0), 
	"SC_NAME" VARCHAR2(45 BYTE), 
	"SC_PHONE" VARCHAR2(45 BYTE), 
	"SC_DAY" VARCHAR2(10 BYTE), 
	"SC_TIME" VARCHAR2(50 BYTE), 
	"SC_STREET" VARCHAR2(45 BYTE), 
	"SC_CITY" VARCHAR2(45 BYTE), 
	"SC_STATE" VARCHAR2(45 BYTE), 
	"SC_POSTCODE" VARCHAR2(45 BYTE), 
	"SC_COUNTRY" VARCHAR2(45 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table MARKERS
--------------------------------------------------------

  CREATE TABLE "43714994"."MARKERS" 
   (	"MARK_ID" NUMBER(*,0), 
	"SHOPPER_ID" NUMBER(*,0), 
	"SC_NUM_ID" NUMBER(*,0), 
	"MARK_NAME" VARCHAR2(60 BYTE), 
	"MARK_STREET" VARCHAR2(45 BYTE), 
	"MARK_CITY" VARCHAR2(45 BYTE), 
	"MARK_STATE" VARCHAR2(45 BYTE), 
	"MARK_POSTCODE" VARCHAR2(45 BYTE), 
	"MARK_COUNTRY" VARCHAR2(45 BYTE), 
	"MARK_LAT" FLOAT(126), 
	"MARK_LNG" FLOAT(126)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table PRODUCT
--------------------------------------------------------

  CREATE TABLE "43714994"."PRODUCT" 
   (	"PROD_ID" NUMBER(*,0), 
	"PROD_NAME" VARCHAR2(40 BYTE), 
	"PROD_DESC" VARCHAR2(128 BYTE) DEFAULT NULL, 
	"PROD_IMG_URL" VARCHAR2(128 BYTE) DEFAULT NULL, 
	"PROD_LONG_DESC" VARCHAR2(256 BYTE) DEFAULT NULL, 
	"PROD_SKU" CHAR(16 BYTE) DEFAULT NULL, 
	"PROD_DISP_CMD" VARCHAR2(128 BYTE) DEFAULT NULL, 
	"PROD_WEIGHT" NUMBER(6,0), 
	"PROD_L" NUMBER(*,0), 
	"PROD_W" NUMBER(*,0), 
	"PROD_H" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table LOG
--------------------------------------------------------

  CREATE TABLE "43714994"."LOG" 
   (	"ID" NUMBER(*,0), 
	"LOG_SHOPPER_ID" NUMBER(*,0), 
	"LOG_CMD_ID" NUMBER(*,0), 
	"LOG_CAT_ID" NUMBER(*,0), 
	"LOG_PROD_ID" NUMBER(*,0), 
	"LOG_TIMESTAMP" TIMESTAMP (6)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into "43714994".CATEGORY
SET DEFINE OFF;
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (1,'Store root category','Store root category',null,'DisplayCategory.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (2,'Men''s Clothing','Clothing of all types - trousers, jackets, etc.',null,'DisplayCategory.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (3,'Jeans','Jeans: Denim, cotton, etc.',null,'DisplayCategory.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (4,'Denim Jeans','Denim jeans',null,'DisplayCategory.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (5,'Boot cut jeans','Boot cut jeans',null,'DisplayProducts.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (6,'Straight cut jeans','Straight cut jeans',null,'DisplayProducts.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (7,'Shoes and boots','Shoes and boots','shoes.jpg','DisplayCategory.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (8,'Computers','Computers','computers.jpg','DisplayCategory.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (9,'Laptop computers','Laptop computers','laptop.jpg','DisplayProducts.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (10,'Servers','Rackmount and tower servers','server.jpg','DisplayProducts.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (11,'Shirts','Shirts of all kinds','shirts.jpg','DisplayCategory.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (12,'Trousers','Trousers, jeans, shorts','trousers.jpg','DisplayCategory.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (13,'Jackets','Sports jackets, blazers, etc.','jackets.jpg','DisplayCategory.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (14,'Business shirts','Cotton and cotton blend business shirts','bshirt.jpg','DisplayCategory.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (15,'Short-sleeved shirts','With collars and short sleeves','sshirt.jpg','DisplayCategory.php');
Insert into "43714994".CATEGORY (CAT_ID,CAT_NAME,CAT_DESC,CAT_IMG_URL,CAT_DISP_CMD) values (16,'T-shirts','Tees: plain and with designs','tshirt.jpg','DisplayCategory.php');
REM INSERTING into "43714994".SESSIONS
SET DEFINE OFF;
REM INSERTING into "43714994".SHOPPER
SET DEFINE OFF;
REM INSERTING into "43714994".STORECONTACT
SET DEFINE OFF;
Insert into "43714994".STORECONTACT (SC_NUM_ID,MARK_ID,SC_NAME,SC_PHONE,SC_DAY,SC_TIME,SC_STREET,SC_CITY,SC_STATE,SC_POSTCODE,SC_COUNTRY) values (1,1,'Macquarie Shopping Centre','(02) 9887 0800','Mon-Sun','8.30am - 8pm','Cnr Herring and Waterloo Roads','North Ryde','NSW','2113','Australia');
Insert into "43714994".STORECONTACT (SC_NUM_ID,MARK_ID,SC_NAME,SC_PHONE,SC_DAY,SC_TIME,SC_STREET,SC_CITY,SC_STATE,SC_POSTCODE,SC_COUNTRY) values (2,2,'Broadway Shopping Centre','(02) 8398 5620','Mon-Sun','10am - 7pm','1 Bay Street','Broadway','NSW','2007','Australia');
Insert into "43714994".STORECONTACT (SC_NUM_ID,MARK_ID,SC_NAME,SC_PHONE,SC_DAY,SC_TIME,SC_STREET,SC_CITY,SC_STATE,SC_POSTCODE,SC_COUNTRY) values (3,3,'Westfield Sydney','(02) 8236 9200','Mon-Fri','9.30am - 7pm','Pitt Street & Market Street','Sydney','NSW','2000','Australia');
Insert into "43714994".STORECONTACT (SC_NUM_ID,MARK_ID,SC_NAME,SC_PHONE,SC_DAY,SC_TIME,SC_STREET,SC_CITY,SC_STATE,SC_POSTCODE,SC_COUNTRY) values (4,4,'Westfield Chatswood','(02) 9412 1555','Mon-Fri','9am - 5.30pm','1 Anderson Street','Chatswood','NSW','2067','Australia');
Insert into "43714994".STORECONTACT (SC_NUM_ID,MARK_ID,SC_NAME,SC_PHONE,SC_DAY,SC_TIME,SC_STREET,SC_CITY,SC_STATE,SC_POSTCODE,SC_COUNTRY) values (5,5,'Top Ryde Shopping Centre ','(02) 9808 8888','Mon-Sat','9am - 6pm','Devlin Street & Blaxland Road','Ryde','NSW','2112','Australia');
Insert into "43714994".STORECONTACT (SC_NUM_ID,MARK_ID,SC_NAME,SC_PHONE,SC_DAY,SC_TIME,SC_STREET,SC_CITY,SC_STATE,SC_POSTCODE,SC_COUNTRY) values (6,6,'Westfield Eastgardens','(02) 9344 6766','Mon-Fri','9am - 5.30pm','152 Bunnerong Road','Eastgardens','NSW','2036','Australia');
Insert into "43714994".STORECONTACT (SC_NUM_ID,MARK_ID,SC_NAME,SC_PHONE,SC_DAY,SC_TIME,SC_STREET,SC_CITY,SC_STATE,SC_POSTCODE,SC_COUNTRY) values (7,7,'Westfield Warringah Mall','(02) 9905 0633','Mon-Fri','9am - 5.30pm','Condamine Street & Old Pittwater Road','Brookvale ','NSW','2100','Australia');
Insert into "43714994".STORECONTACT (SC_NUM_ID,MARK_ID,SC_NAME,SC_PHONE,SC_DAY,SC_TIME,SC_STREET,SC_CITY,SC_STATE,SC_POSTCODE,SC_COUNTRY) values (8,8,'Westfield Hornsby','(02) 9477 5111','Mon-Fri','9am - 5.30pm','236 Pacific Highway','Hornsby ','NSW','2077','Australia');
Insert into "43714994".STORECONTACT (SC_NUM_ID,MARK_ID,SC_NAME,SC_PHONE,SC_DAY,SC_TIME,SC_STREET,SC_CITY,SC_STATE,SC_POSTCODE,SC_COUNTRY) values (9,9,'Westfield Carindale','(07) 3120 5400','Mon-Fri','9am - 5.30pm','1151 Creek Road','Carindale','QLD','4152','Australia');
REM INSERTING into "43714994".MARKERS
SET DEFINE OFF;
Insert into "43714994".MARKERS (MARK_ID,SHOPPER_ID,SC_NUM_ID,MARK_NAME,MARK_STREET,MARK_CITY,MARK_STATE,MARK_POSTCODE,MARK_COUNTRY,MARK_LAT,MARK_LNG) values (1,1,1,'Macquarie Shopping Centre','Cnr Herring and Waterloo Roads','North Ryde','NSW','2113','Australia',-33.776553,151.12037);
Insert into "43714994".MARKERS (MARK_ID,SHOPPER_ID,SC_NUM_ID,MARK_NAME,MARK_STREET,MARK_CITY,MARK_STATE,MARK_POSTCODE,MARK_COUNTRY,MARK_LAT,MARK_LNG) values (2,2,2,'Broadway Shopping Centre','1 Bay Street','Broadway','NSW','2007','Australia',-33.8836298,151.1939445);
Insert into "43714994".MARKERS (MARK_ID,SHOPPER_ID,SC_NUM_ID,MARK_NAME,MARK_STREET,MARK_CITY,MARK_STATE,MARK_POSTCODE,MARK_COUNTRY,MARK_LAT,MARK_LNG) values (3,3,3,'Westfield Sydney','Pitt Street & Market Street','Sydney','NSW','2000','Australia',-33.8700461,151.2088597);
Insert into "43714994".MARKERS (MARK_ID,SHOPPER_ID,SC_NUM_ID,MARK_NAME,MARK_STREET,MARK_CITY,MARK_STATE,MARK_POSTCODE,MARK_COUNTRY,MARK_LAT,MARK_LNG) values (4,4,4,'Westfield Chatswood','1 Anderson Street','Chatswood','NSW','2067','Australia',-33.7968921,151.1841106);
Insert into "43714994".MARKERS (MARK_ID,SHOPPER_ID,SC_NUM_ID,MARK_NAME,MARK_STREET,MARK_CITY,MARK_STATE,MARK_POSTCODE,MARK_COUNTRY,MARK_LAT,MARK_LNG) values (5,5,5,'Top Ryde Shopping Centre','Devlin Street & Blaxland Road','Ryde','NSW','2112','Australia',-33.812182,151.1060215);
Insert into "43714994".MARKERS (MARK_ID,SHOPPER_ID,SC_NUM_ID,MARK_NAME,MARK_STREET,MARK_CITY,MARK_STATE,MARK_POSTCODE,MARK_COUNTRY,MARK_LAT,MARK_LNG) values (6,6,6,'Westfield Eastgardens','152 Bunnerong Road','Eastgardens','NSW','2036','Australia',-33.9448107,151.2244195);
Insert into "43714994".MARKERS (MARK_ID,SHOPPER_ID,SC_NUM_ID,MARK_NAME,MARK_STREET,MARK_CITY,MARK_STATE,MARK_POSTCODE,MARK_COUNTRY,MARK_LAT,MARK_LNG) values (7,7,7,'Westfield Warringah Mall','Condamine Street & Old Pittwater Road','Brookvale','NSW','2100','Australia',-33.767719,151.265968);
Insert into "43714994".MARKERS (MARK_ID,SHOPPER_ID,SC_NUM_ID,MARK_NAME,MARK_STREET,MARK_CITY,MARK_STATE,MARK_POSTCODE,MARK_COUNTRY,MARK_LAT,MARK_LNG) values (8,8,8,'Westfield Hornsby','236 Pacific Highway','Hornsby','NSW','2077','Australia',-33.7049459,151.100281);
Insert into "43714994".MARKERS (MARK_ID,SHOPPER_ID,SC_NUM_ID,MARK_NAME,MARK_STREET,MARK_CITY,MARK_STATE,MARK_POSTCODE,MARK_COUNTRY,MARK_LAT,MARK_LNG) values (9,9,9,'Westfield Carindale','1151 Creek Road','Carindale','QLD','4152','Australia',-27.502998,153.101627);
REM INSERTING into "43714994".PRODUCT
SET DEFINE OFF;
Insert into "43714994".PRODUCT (PROD_ID,PROD_NAME,PROD_DESC,PROD_IMG_URL,PROD_LONG_DESC,PROD_SKU,PROD_DISP_CMD,PROD_WEIGHT,PROD_L,PROD_W,PROD_H) values (1,'Null Product','Null product','null.jpg','Null product','NULL            ','DisplayProduct.php',null,null,null,null);
Insert into "43714994".PRODUCT (PROD_ID,PROD_NAME,PROD_DESC,PROD_IMG_URL,PROD_LONG_DESC,PROD_SKU,PROD_DISP_CMD,PROD_WEIGHT,PROD_L,PROD_W,PROD_H) values (2,'Levi 501','Levi 501 Classic Jeans','levi_501.jpg','You will look terrific in these classic blue denim jeans. Hard-wearing, but stylish, these durable pants always look smart, even when you''ve just gotten off your horse.','LEVI501         ','DisplayProduct.php',null,null,null,null);
Insert into "43714994".PRODUCT (PROD_ID,PROD_NAME,PROD_DESC,PROD_IMG_URL,PROD_LONG_DESC,PROD_SKU,PROD_DISP_CMD,PROD_WEIGHT,PROD_L,PROD_W,PROD_H) values (3,'Levi 504','Levi 504 Cord Jeans','levi_504.jpg','Light-weight corduroy is perfect for those hot summer days! Look cool and be cool. Available in blue, brown and black.','LEVI504         ','DisplayProduct.php',null,null,null,null);
Insert into "43714994".PRODUCT (PROD_ID,PROD_NAME,PROD_DESC,PROD_IMG_URL,PROD_LONG_DESC,PROD_SKU,PROD_DISP_CMD,PROD_WEIGHT,PROD_L,PROD_W,PROD_H) values (4,'Levi 502','Levi 502 Jeans','levi_502.jpg','The classic Levi look in black','LEVI502         ','DisplayProduct.php',null,null,null,null);
Insert into "43714994".PRODUCT (PROD_ID,PROD_NAME,PROD_DESC,PROD_IMG_URL,PROD_LONG_DESC,PROD_SKU,PROD_DISP_CMD,PROD_WEIGHT,PROD_L,PROD_W,PROD_H) values (5,'Wrangler CCOR','Wrangler Cowboy Cut Original Fit','wr_ccof.jpg','Available in blue and black, yadda, yadda, yadda','WRCCOF          ','DisplayProduct.php',null,null,null,null);
Insert into "43714994".PRODUCT (PROD_ID,PROD_NAME,PROD_DESC,PROD_IMG_URL,PROD_LONG_DESC,PROD_SKU,PROD_DISP_CMD,PROD_WEIGHT,PROD_L,PROD_W,PROD_H) values (6,'XKCD Stats Class Tee','The classic XKCD "Causality" t-shirt','xkcd-t.jpg','The classic XKCD cartoon now comes to a t-shirt near you! Available in blue only.','XKCDT           ','DisplayProduct.php',null,null,null,null);
Insert into "43714994".PRODUCT (PROD_ID,PROD_NAME,PROD_DESC,PROD_IMG_URL,PROD_LONG_DESC,PROD_SKU,PROD_DISP_CMD,PROD_WEIGHT,PROD_L,PROD_W,PROD_H) values (7,'IBM 306m','IBM X-Series 306m 1U Rack-mount server','306m.jpg','A 1U rack-mount server. Specify processor, RAM, storage, etc. below','IBM306M         ','DisplayProduct.php',null,null,null,null);
REM INSERTING into "43714994".LOG
SET DEFINE OFF;
