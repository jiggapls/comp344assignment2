<?php
$title="Store Locator"; 
include("includes/header.php"); 
include_once("includes/controller.php");
?>

  <body class="storelocator">

	<?php 
		require("includes/navigation.php"); 
    ?>

    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <h1>Store Locator</h1>
      </div>
      
      <!-- Search box and Use Current Location Button --!>
      <div class="row">
      	<div class="col-sm-9" id="store-search-box">
      		<div>
      			<div class="input-group stylish-input-group">
      				<input type="search" placeholder="Enter Suburb or Postcode" class="form-control" id="search-bar">
      				<span class="input-group-addon">
      					<button type="submit">
      						<span class="glyphicon glyphicon-search"></span>
      					</button>
      				</span>
      			</div>
      		</div>
      	</div>
      	
			<div class="col-sm-2" id="current-location-box">
      			<button type="button" class="btn btn-default" id="current-location">
      				<span class="glyphicon glyphicon-map-marker"></span> Use Current Location
      			</button>
      		</div>
      </div>
      
      <div id="map-container" class="container-fluid">
      
      	<div class="row">
      		<div id="map" class="col-sm-7"></div>
      		
      		<!-- store locations container -->
      		<div class="col-sm-5">
      			<?php
      				$rows = getStoreLocations($dbo);
      				foreach($rows as $row):
      			?>
      				
      				<div class="col-sm-12" id="store-locations">
      					<p class="location-heading"> <?php echo $row['SC_NAME']; ?> </p>
      						<?php 
      							echo '<p class="distance">'; 
      							echo $row['SC_STREET'].', '. $row['SC_CITY'].' '.$row['SC_STATE'].' '.$row['SC_POSTCODE'].'<br>'.$row['SC_PHONE'].'<br>'.$row['SC_DAY'].' '.$row['SC_TIME'].'<br><hr>'; 
      							echo '</p>';
      						?> 
      				</div>
      			<?php endforeach; ?> 
      		</div>
      	</div>
      	
      </div>
    </div>
    
    <?php require("includes/footer.php"); ?>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-Ta4Bd6noxHDMZEFfxUsvyIDmNPrV_-8&callback=initMap"
  type="text/javascript"></script>
    <script type="text/javascript">
    
    	var map;
		var locationBtn = document.getElementById("current-location");
		
		//Retrieve markers from database
    	var lat = <?php getLatitude($dbo) ?>;
    	var lng = <?php getLongitude($dbo) ?>;

		function initMap() {
    		var map = new google.maps.Map(document.getElementById('map'), {
        		center: {lat: -34.397, lng: 150.644},
        		zoom: 6
    	});
    	var infoWindow = new google.maps.InfoWindow();

    	// Try HTML5 geolocation.
    	locationBtn.onclick = function() { 
        	if (navigator.geolocation) {
          		navigator.geolocation.getCurrentPosition(function(position) {
            		var pos = {
              			lat: position.coords.latitude,
              			lng: position.coords.longitude
            		};
            		map.setCenter(pos);
            		map.setZoom(15);
            		calculateDistance(position);
          		}, function() {
            		handleLocationError(true, infoWindow, map.getCenter());
          		});
        	} else {
          		// Browser doesn't support Geolocation
          		handleLocationError(false, infoWindow, map.getCenter());
        	}
    	};
    	
    	var infoWindowContent = <?php getInfoWindowContent($dbo) ?>;
    
    	var bounds = new google.maps.LatLngBounds();
    	
    	// Display multiple markers on a map
    
    	for( i = 0; i < lat.length; i++ ) {
        	var position = new google.maps.LatLng(lat[i], lng[i]);
        	bounds.extend(position);
        	var marker = new google.maps.Marker({
            	position: position,
            	map: map,
    		}); 
    	
            // Allow each marker to have an info window   
            var infoWindow = new google.maps.InfoWindow(), marker, i;
             
    		google.maps.event.addListener(marker, 'click', (function(marker, i) {
        		return function() {
            		infoWindow.setContent("<h5>"+infoWindowContent[i][0]+"</h5>"+"<p>"+infoWindowContent[i][1]+"<br>"+infoWindowContent[i][2]+" "+infoWindowContent[i][3]+" "+infoWindowContent[i][4]+"</p>");
            		infoWindow.open(map, marker);
        		}
    		})(marker, i));

    		// Automatically center the map fitting all markers on the screen
        		map.fitBounds(bounds); 
		}
		
		// Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    	var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        	this.setZoom(11);
        	google.maps.event.removeListener(boundsListener);
    	});
    	
}

function calculateDistance(position) {
    var myLat = position.coords.latitude;
    var myLng = position.coords.longitude;
    

    var rad = function(x) {
        return x * Math.PI / 180;
    };

    var R = 6371; // Earth’s mean radius in km

    for( i = 0; i < lat.length; i++ ) {
        //Calculate distance from user's location to the stores
        var dLat = rad(lat[i] - myLat);
        var dLong = rad(lng[i] - myLng);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(myLat)) * Math.cos(rad(lat[i])) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var distance = R * c; // returns the distance in km
        var distance = distance.toFixed(2);
        //Print Stores Location
        var x = document.getElementsByClassName("distance")[i];
        x.innerHTML += distance + " km away";
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                            'Error: The Geolocation service failed.' :
                            'Error: Your browser doesn\'t support geolocation.');
}

    </script>
    
  </body>
</html>