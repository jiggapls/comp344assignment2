    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
  		  	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            	<span class="sr-only">Toggle navigation</span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
          	</button>
          	<a class="navbar-brand" href="index.php">Mobile Pilot</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="index.php" class="home">Home</a></li>
				<li><a href="StoreLocator.php" class="storelocator">Store Locator</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class=""><a href="http://comp344.com/LoginShopper.php">Login</a></li>
				<li class=""><a href="Register.php">Register</a></li>      
			</ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>