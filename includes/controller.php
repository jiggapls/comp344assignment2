<?php
/* includes database */
require_once("common_db.php");

# connect to db via PDO provided file from COMP344 iLearn page
$dbo = db_connect();

/* attempt to query database or die function */
function try_or_die($sql) {
	try {
		$sql->execute();
	} catch (PDOException $ex) {
		echo $ex->getMessage();
		die ("Invalid Query");
	}
}

/* retrieve store locations from database */
function getStoreLocations($dbo) {
	$sql = $dbo->prepare("SELECT sc_name, sc_phone, sc_day, sc_time, sc_street, sc_city, sc_state, sc_postcode FROM StoreContact");

	try_or_die($sql);
	
	while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
		$rows[] = $row;
	}
	
	return $rows;
}

function getLatitude($dbo) {
	$sql = $dbo->prepare("SELECT mark_lat FROM markers");
	
	try_or_die($sql);
	
	while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
		
		$latitudes[] = $row['MARK_LAT'];	
	}
	
	echo json_encode($latitudes);	
}

function getLongitude($dbo) {
	$sql = $dbo->prepare("SELECT mark_lng FROM markers");
	
	try_or_die($sql);
	
	while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
	
		$longitudes[] = $row['MARK_LNG'];	
	}
	
	echo json_encode($longitudes);
}

function getInfoWindowContent($dbo) {
	$sql = $dbo->prepare("SELECT mark_name, mark_street, mark_city, mark_state, mark_postcode FROM markers");
	
	try_or_die($sql);
	
	while ($row = $sql->fetch(PDO::FETCH_NUM)) {
		$rows[] = $row;
		
	}
	
	echo json_encode($rows);

}

?>